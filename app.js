// Setup
var express = require('express');
var app = express();

var mongoose = require('mongoose');
mongoose.connect("mongodb://localhost:27017/node-blog",{ useNewUrlParser: true });
mongoose.set('useCreateIndex', true);

var bodyParser = require('body-parser');

var session = require('express-session');

var bcrypt = require('bcrypt');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));
app.use(session({secret: 'keyboard cat',saveUninitialized: false, resave: true,}));

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

var timeObject={hours:String, minutes:String, seconds:String, day:String, date:String, month:String,year:String}

var postSchema = new mongoose.Schema({  title:String,
										shortbody:String,
										body: String,
										authorID:String,
										dateofcreation:{hours:String, minutes:String, seconds:String, day:String, date:String, month:String,year:String},
										editdate:{hours:String, minutes:String, seconds:String, day:String, date:String, month:String,year:String}
									});

var userSchema = new mongoose.Schema({  name:{type:String,unique:true,required:true,trim:true},
                    					email:{type:String,unique:true,required:true,trim:true},
                    					password: {type:String,unique:true,required:true,trim:true} 
                });


//hashing a password before saving it to the database
userSchema.pre('save', function (next) {
  var user = this;
  bcrypt.hash(user.password, 10, function (err, hashed){
    if (err) {
      return next(err);
    }
    user.password = hashed;
    next();
  })
});

var Post = mongoose.model('Post', postSchema);
var User = mongoose.model('User', userSchema);
//-----------------------------------------------------------------------------------------
// Routes
app.get("/registration", (req, res) => {
    res.render('registration');
});

app.get("/", (req, res) => {
   Post.find({}, (err, posts) => {
      res.render('index', { posts: posts})
   });
});
//find the logged in user
app.get("/userloggedin", (req, res) => {
   User.findById(req.session.userId, (err, user) => {
   	  if(req.session.userId){
   	  	console.log(user.email);
   	  }else{
   	  	console.log("No user logged in");
   	  }
      res.redirect('/');
   });
});

app.get("/login", (req, res) => {
   Post.find({}, (err, posts) => {
      res.render('login', {})
   });
});

app.get("/editprofile", (req, res) => {
   User.findById({_id:req.session.userId}, (err, user) => {
   		if(req.session.userId){
   			res.render('editprofile', {user:user})
   		}else{
   			res.redirect('/login');
   		}
      	
   });
});

app.get("/myposts", (req, res) => {
   Post.find({authorID:req.session.userId}, (err, posts) => {
   		console.log(posts.length)
   		if(req.session.userId){
   			res.render('index', { posts: posts})
   		}
   		else{//if not logged in
   			res.redirect('/login');
   		}   	
    });

});

// GET /logout
app.get('/logout', function(req, res, next) {
  if (req.session.userId) {
    // delete session object
    req.session.destroy(function(err) {
      if(err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
    console.log("YOU LOGGED OUT")
  }
  else{
  	//something else
  	console.log("YOU ARE NOT LOGGED IN")
  	return res.redirect('/');
  }
});
//-----------------------------------------------------------------------------------------
app.post("/submitlogin", (req, res) => {
   User.findOne( {$or: [ {name:req.body.nameoremail} , {email:req.body.nameoremail} ]}, (err, user) => {
	   	if(user){
	   		bcrypt.compare(req.body.password, user.password, function (err, result) {
	        if (result === true) {
	        	req.session.userId = user._id;
	   			console.log(req.session.userId);
	      		res.redirect('/');
	        } else {
	        	console.log("error:Wrong Password")
	   		    res.redirect('/login');
	        }
	      })
	   	}else{
	   		console.log("error:username or email is wrong")
	   		res.redirect('/login');
	   	}     
   });

});

app.post('/addpost', (req, res) => {
	if(req.session.userId){
		console.log("YOU CAN MAKE A POST");
		req.body.authorID=req.session.userId;
		req.body.dateofcreation=todayFormated();
		req.body.editdate=todayFormated();
	    var postData = new Post(req.body);
	    console.log(req.body);
	    postData.save().then( result => {
	    	//view users posts
	        res.redirect('/myposts');
	    }).catch(err => {
	        res.status(400).send("Unable to save data!");
	    });
	}else{
		//you must login first
		res.redirect('/login');
	}

});
//submit user registration form
app.post('/createuser', (req, res) => {
	User.findOne( {$or: [ {name:req.body.name} , {email:req.body.email} ]}, (err, user) => {
	   	if(user){
	   		console.log("this username or email already exists!");
	      	res.redirect('/registration');
	   	}else{
		    var userData = new User(req.body);   
		    userData.save().then( result => {
		        res.redirect('/');
		    }).catch(err => {
		        res.status(400).send("Unable to save data!");
		    });
		    req.session.userId = userData._id;
		    console.log(req.session.userId);
	   	}   
    });
});

app.post("/deletepost", (req, res) => {
	console.log(req.body._id);
	Post.deleteOne({ _id: req.body._id }, function (err) {
  		if (err) return handleError(err);
	});
	res.redirect('/');
});


app.post("/editpost", (req, res) => {
   Post.find({ _id: req.body._id }, (err, posts) => {
      res.render('editpage', { posts: posts})
   });
});

app.post("/editsave", (req, res) => {
	Post.updateOne({ _id: req.body._id },{$set:{"title":req.body.title, "shortbody":req.body.shortbody,"body":req.body.body}}, function (err) {
  		if (err) return handleError(err);
	});
	res.redirect('/');
});

app.post("/saveprofilechanges", (req, res) => {
	User.updateOne({ _id: req.session.userId },{$set:{"name":req.body.name, "email":req.body.email}}, function (err) {
  		if (err) return handleError(err);
	});

	res.redirect('/');
}); 
//help functions
function todayFormated(){
	var today = new Date();
	var hr = today.getHours();
	var min = today.getMinutes();
	var sec = today.getSeconds();
	var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	var day = days[today.getDay()];
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	if(dd<10){
	    dd='0'+dd;
	} 
	if(mm<10){
	    mm='0'+mm;
	} 
	//var today = hr+':'+min+':'+sec+'-'+day+' '+dd+'/'+mm+'/'+yyyy;
	var today={hours:hr,minutes:min,seconds:sec,day:day,date:dd,month:mm,year:yyyy}
	return today;
}


// Listen
app.listen(3000, () => {
    console.log('Server listing on 3000');
})